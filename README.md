# Public Issue Board

Welcome to the official Z-Ray Entertainment Issue Board.  
If you need and form of assitance be it for software we develop or any YouTube Video we made. Feel free to open up an issue here.  

Also this repositoryy will serve as a wiki for common issues and how to solve them.

Kind regards,  
V.

[YouTube - Main](https://www.youtube.com/@ZRayEntertainment)  
[YouTube - Gaming](https://www.youtube.com/@RoboNTux)  
[YouTube - Guides](https://www.youtube.com/@RoboNTuxGuides)  
[YouTube - Benchamrks](https://www.youtube.com/@RoboTuxBenchmarks)

## Space Conflicts - Empires
If you have questions about or an issue with our game Space Conflicts - Empires, [please visit this issue board instead](https://gitlab.com/z-ray-entertainment/space-conflicts-empires-public-issue-board).